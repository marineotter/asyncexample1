﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace AsyncExample1
{
    public partial class Form1 : Form
    {
        // ワーカースレッドを保持しておく
        Thread workerThread = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void heavyTask()
        {
            // 10秒かかる処理の例
            System.Console.WriteLine("Start Heavy Task...");
            Thread.Sleep(10000);
            System.Console.WriteLine("End Heavy Task...");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Console.WriteLine("Normal Task1");

            // workerThreadオブジェクト生きてたら実行しない。
            if (this.workerThread != null && this.workerThread.IsAlive)
            {
                System.Console.WriteLine("Heavy Task is already running. Nothing to do..");
            }
            else
            {
                // 10秒かかる処理を開始する　←完了を待たない点に注意！
                this.workerThread = new Thread(new ThreadStart(heavyTask));
                workerThread.Start();
            }

            System.Console.WriteLine("Normal Task3");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Console.WriteLine("Normal Task2");
        }
    }
}
